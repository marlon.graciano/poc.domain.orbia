package domain.contracts

import domain.dto.MusicDto
import domain.entities.Music
import mysql.IBaseRepository

interface IMusicRepository : IBaseRepository<Music, MusicDto> {
    fun getByPlaylistId(playlistId: Long) : List<Music>
    fun getByPlaylistIdAndMusicId(playlistId: Long, musicId: Long) : Music
}
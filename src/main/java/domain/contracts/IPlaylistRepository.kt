package domain.contracts

import domain.dto.PlaylistDto
import domain.entities.Playlist
import mysql.IBaseRepository

interface IPlaylistRepository : IBaseRepository<Playlist, PlaylistDto> {
}
package domain.dto

import java.util.*

class PlaylistDto {
    var id: Long = 0
    var name: String = ""
    var description: String = ""
    var musics: MutableList<MusicDto> = ArrayList()
}
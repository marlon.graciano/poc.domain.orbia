package domain.dto

class MusicDto {

    var id: Long = 0
    lateinit var band: String
    lateinit var note: String
    lateinit var playlist: PlaylistDto
}
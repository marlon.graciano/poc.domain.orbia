package domain.implementations

import domain.contracts.IMusicRepository
import domain.dto.MusicDto
import domain.entities.Music
import mysql.BaseRepository
import org.springframework.stereotype.Service
import javax.persistence.EntityManager

@Service
class MusicRepository : IMusicRepository, BaseRepository<Music, MusicDto>() {

    override fun getByPlaylistId(playlistId: Long): List<Music> {
        return em!!.createQuery("select m from Music m where m.playList.id = :playlistId", Music::class.java)
                .setParameter("playlistId", playlistId)
                .resultList
    }

    override fun getByPlaylistIdAndMusicId(playlistId: Long, musicId: Long) : Music {
        return em!!.createQuery("select m from Music m where m.playList.id = :playlistId and m.id = :musicId", Music::class.java)
                .setParameter("playlistId", playlistId)
                .setParameter("musicId", musicId)
                .singleResult
    }
}
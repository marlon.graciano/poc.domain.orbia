package domain.implementations

import domain.contracts.IPlaylistRepository
import domain.dto.PlaylistDto
import domain.entities.Playlist
import mysql.BaseRepository
import org.springframework.stereotype.Service
import javax.persistence.EntityManager

@Service
class PlaylistRepository : IPlaylistRepository, BaseRepository<Playlist, PlaylistDto>() {

}
package domain.entities

import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
@Table(name = "playlists")
class Playlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long? = null

    @NotBlank
    @Size(min = 2, max = 60)
    @Column(nullable = false, length = 60)
    private lateinit var name: String

    @NotBlank
    @Column(nullable = false)
    private lateinit var description: String

    @OneToMany(mappedBy = "playList", cascade = [CascadeType.ALL])
    private lateinit var musics: MutableList<Music>

    fun getId(): Long? {
        return id
    }

    fun getName() : String {
        return name
    }

    fun getDescription() : String {
        return description
    }

    fun addName(name: String) {
        this.name = name
    }

    fun addDescription(description: String) {
        this.description = description
    }

    fun addMusic(music: Music) {
        musics.add(music)
    }
}
package domain.entities

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.hibernate.validator.constraints.Range
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
@Table(name = "musics")
class Music {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private var id: Long? = null

    @NotBlank
    @Size(min = 2, max = 50)
    @Column(nullable = false, length = 50)
    private lateinit var title: String

    @NotBlank
    @Size(min = 2, max = 50)
    @Column(nullable = false, length = 50)
    private lateinit var band: String

    @NotBlank
    @Range(min = 2, max = 10)
    @Column(nullable = false)
    private lateinit var note: String

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "playlist_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private lateinit var playList: Playlist

    fun getId() : Long? {
        return id
    }

    fun addTitle(title: String) {
        this.title = title
    }

    fun addBand(band: String) {
        this.band = band
    }

    fun addNote(note: String) {
        this.note = note
    }

//    fun addPlaylist(playList: Playlist) {
//        this.playList = playList
//    }
}
